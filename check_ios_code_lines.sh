#!/bin/sh
#  Copyright (c) 2013 Ctrip.com. All rights reserved.

current_path=`pwd`

echo "=======Flight======="
cd $current_path
cd ./CTRIP_WIRELESS/CTRIP_WIRELESS/Class/UI/CTFlight
flitght1=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Sender/CtripWireless_Sender/classes/CTFlight
flitght2=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Business/CtripWireless_Business/classes/business/flight
flitght3=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Business/CtripWireless_Business/classes/business/flightCommon
flitght4=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
let flight=$flitght1+$flitght2+$flitght3+$flitght4
echo "Flight: $flight"

echo "\n=======Hotel======="
cd $current_path
cd ./CTRIP_WIRELESS/CTRIP_WIRELESS/Class/UI/CTHotelPage
hotel1=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Sender/CtripWireless_Sender/classes/sender/hotel
hotel2=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Sender/CtripWireless_Sender/classes/viewcache/hotel
hotel3=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Business/CtripWireless_Business/classes/business/hotel
hotel4=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
let hotel=$hotel1+$hotel2+$hotel3+$hotel4
echo "Hotel: $hotel"

echo "\n=======Destionation======="
cd $current_path
cd ./CTRIP_WIRELESS/CTRIP_WIRELESS/Class/UI/CTDestination
d1=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Sender/CtripWireless_Sender/classes/viewcache/distination
d2=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Business/CtripWireless_Business/classes/business/district
d3=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Business/CtripWireless_Business/classes/business/districtEx
d4=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
let destination=$d1+$d2+$d3+$d4
echo "Destionation: $destination"

echo "\n=======Train======="
cd $current_path
cd ./CTRIP_WIRELESS/CTRIP_WIRELESS/Class/UI/CTTrain
t1=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Sender/CtripWireless_Sender/classes/sender/train
t2=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Sender/CtripWireless_Sender/classes/viewcache/train
t3=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
cd ./CtripWireless_Business/CtripWireless_Business/classes/business/train
t4=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
cd $current_path
let train=$t1+$t2+$t3+$t4
echo "Train: $train"


echo "\n=======All======="
cd $current_path
total=`find ./ -name "*.m" -exec wc -l {} \; | awk '{a+=$1}END{print a}' | awk '{a+=$1}END{print a}' `
echo "Total: $total"

let public=$total-$train-$destination-$hotel-$flight
echo "\n=======Public======="
echo "Public: $public"
