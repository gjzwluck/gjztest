#!/bin/sh
#  Copyright (c) 2014 Ctrip.com. All rights reserved.

cd ../CTRIP_WIRELESS

#参数判断
if [ $# -lt 2 ]; then
    echo "举例：$0 5.4 sit1 ida"
    echo "第1个参数表示号标记(必填)：例如5.4 "
    echo "第2个参数表示版本标记(必填)：例如sit1 "
    exit     
fi

#配置参数
versionName="$1"                            #大版本标记
subVersionName="$2"                         #小版本标记
configurationName="Release"      

#辅助参数
targetName="CTRIP_WIRELESS"                 #项目名称(xcode左边列表中显示的项目名称)
sdkName="iphoneos7.1"
scriptDir=$(cd "$(dirname "$0")"; pwd)
echo `pwd`
cd "${scriptDir}"
echo `pwd`
curDir=`pwd`                                #当前目录
distDir="${curDir}/build"                   #发布包目录
buildDir="${curDir}/build"                  #build目录
businessDir="../CtripWireless_Business"     #Business工程目录
senderDir="../CtripWireless_Sender"         #Sender工程目录
echo "In the realscript:${TARGETPLATFORM}"
if [ "${TARGETPLATFORM}" = "SIMULATOR" ]; then
    sdkName="iphonesimulator7.1"
fi

#计算参数
xcodebuildParams="-workspace ${targetName}.xcodeproj/project.xcworkspace -scheme ${targetName} -configuration ${configurationName} -sdk ${sdkName} build -derivedDataPath ${buildDir}"

echo $xcodebuildParams

#清除
rm -rdf "$buildDir"
rm -rdf "$businessDir/build/"
rm -rdf "$senderDir/build/"

rm -rdf "$distDir"
mkdir "$distDir"

#clean
echo "*** Clean old Build files ***"
xcodebuild clean -configuration $configurationName 

#list configuration and schema
echo "*** List Xcode Configurtion ***"
xcodebuild -list        

if [ "${TARGETPLATFORM}" = "SIMULATOR" ]; then
appfile="${buildDir}/Build/Products/${configurationName}-iphonesimulator/${targetName}.app"
else
appfile="${buildDir}/Build/Products/${configurationName}-iphoneos/${targetName}.app"
fi

ipapath="${distDir}/${targetName}_${versionName}_${subVersionName}.ipa"

#build app
echo "*** Start build app***"

#GCC_PREPROCESSOR_DEFINITIONS="IN_PRODUCT_ENV=1"
if [ "${TARGETPLATFORM}" = "SIMULATOR" ]; then
xcodebuild $xcodebuildParams VALID_ARCHS="i386"  || exit 1
else
xcodebuild $xcodebuildParams || exit 2
fi


if [ "${TARGETPLATFORM}" = "SIMULATOR" ]; then
    echo "模拟器不需要生产IPA"
else
    #build ipa package
    echo "*** Start pack ipa ***"
    /usr/bin/xcrun -sdk "$sdkName" PackageApplication -v "$appfile" -o "$ipapath" || exit 4
fi

echo "*** END ***"
exit 0
