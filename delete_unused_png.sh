#!/bin/sh
#  Copyright (c) 2013 Ctrip.com. All rights reserved.

list="png_list"
for line in `cat $list`
do
    if [[ $line =~ @2x.png ]]; then
        line="${line}@"
    fi
    svn rm $line
done

